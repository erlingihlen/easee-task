# README #

### notes and goals for improving this program
Main focus of the improvements is to make the code readable, and easy to expand on

must dos:

- [x] extract each command into separate methods
- [x] move inventory to be a field on the machine, not a variable in the start scope
- [x] add price as a property of the soda
- [x] don’t have specific flows per product, make it generic, easy to add new products to the machine

improvements:

- [x] change inventory to be a map
    - [x] not really relevant for the size of the data in the sample program, but keeps the lookup speed on a product constant, doesn’t require searching in a list
- [ ] make it easy to add new functionality
    - [ ] maybe figure out some generic way to expose available commands to the user 
- [ ] tests
    - [ ] depending on the domain it might make sense to create tests before refactoring/remaking functinoality to make it less likely that you are introducing unknown breaking changes
    - [ ] due to it being a console program, and just logging as it goes as feedback for the user, I would redesign the structure of the program to make it testable in a meaningful way
- [x] depending on how much you want to expand the program, it might be a good idea to extract the classes to separate files
    - [x] I just do it in general for the sake of expanding programs should be easier
- [x] clear unused imports
- [ ] input sanitization
    - [ ] tryparse for integer wasn’t available for this version of net framework, so instead of doing try/cath I left it for now


nice to haves:

- [ ] since it’s a console application, I will leave all the console logging for now
    - [ ] ideally the sodamachine would return some output that the console app would interprit and inform what it means to the user like in other user interfaces, instead of “logging as we go”
- [ ] at the moment, every available command is manually logged to inform the user what commands and type of input are available
    - [ ] perhaps we could implement some generic command, and through configuration, or c# wizardry, populate available commands for the soda machine and make a generic way to show them to the user, as well as determining what method to select in the HandleCommand

code snippit to show concept of making available commands generic and configurable
```
{
    Command = "order",
    CommandDescription = "Order from machines buttons",
    InputDescription = "$availableProducts",
    MethodToInvoke = OrderSoda
}
...
var informedAction = $"{action.Command} ({action.InputDescription}) - {action.CommandDescription}";
Console.WriteLine(informedAction.Replace("$availableProducts", availableProductsPrinted));
...
action.MethodToInvoke.Invoke(input);
```