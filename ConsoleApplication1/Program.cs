﻿using System.Collections.Generic;
using ConsoleApplication1.Domains.SodaMachine;
using ConsoleApplication1.Domains.SodaMachine.Contracts;

namespace ConsoleApplication1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            SodaMachine sodaMachine = new SodaMachine();
            sodaMachine.ResetStock();
            sodaMachine.Start();
        }
    }
}
