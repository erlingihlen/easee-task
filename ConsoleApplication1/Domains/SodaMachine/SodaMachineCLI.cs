using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleApplication1.Domains.SodaMachine.Contracts;

namespace ConsoleApplication1.Domains.SodaMachine
{
    public class SodaMachine
    {
        private int money;
        private Dictionary<string, Soda> inventory;

        public SodaMachine()
        {
            money = 0;
            inventory = new Dictionary<string, Soda>();
        }

        public void ResetStock(List<Soda> optionalCustomStock = null)
        {
            // either restock it with specified input, or default stock
            inventory.Clear();
            var inventoryInput = optionalCustomStock ?? new List<Soda>
            {
                new Soda { Name = "coke", Nr = 5, Price = 20}, 
                new Soda { Name = "sprite", Nr = 3 , Price = 15}, 
                new Soda { Name = "fanta", Nr = 3, Price = 15},
                new Soda { Name = "special", Nr = 1, Price = 100},
            };
            inventoryInput.ForEach(soda => inventory.Add(soda.Name, soda));
        }

        public void Start()
        {
            var availableProdcuts = inventory.Keys.ToList();
            var availableProductsPrinted = String.Join(", ", availableProdcuts);

            while (true)
            {
                // check readme for alternative approach
                Console.WriteLine("\n\nAvailable commands:");
                Console.WriteLine("insert (money) - Money put into money slot");
                Console.WriteLine($"order ({availableProductsPrinted}) - Order from machines buttons");
                Console.WriteLine($"sms order ({availableProductsPrinted}) - Order sent by sms");
                Console.WriteLine("recall - gives money back");
                Console.WriteLine("-------");
                Console.WriteLine($"Inserted money: {money}");
                Console.WriteLine("-------\n\n");

                var input = Console.ReadLine();
                HandleCommand(input);
            }
        }

        private void HandleCommand(string input)
        {
            var split = input.Split(' ');
            var commandArgument = split[split.Length - 1];
            if (input.StartsWith("insert"))
            {
                InsertCash(int.Parse(commandArgument));
            }
            else if (input.StartsWith("order"))
            {
                OrderSoda(commandArgument);
            }
            else if (input.StartsWith("sms order"))
            {
                SmsOrderSoda(commandArgument);
            }
            else if (input.Equals("recall"))
            {
                RecallCash();
            }
        }

        private void InsertCash(int cash)
        {
            money += cash;
            Console.WriteLine($"Adding {cash} to credit");
        }

        private void OrderSoda(string sodaName)
        {
            if (!inventory.ContainsKey(sodaName))
            {
                Console.WriteLine("No such soda");
                return;
            }
            
            var soda = inventory[sodaName];
            if (soda.Nr == 0)
            {
                Console.WriteLine($"No {soda.Name} left");
                return;
            }
            if (money < soda.Price)
            {
                Console.WriteLine($"Need {soda.Price - money} more");
                return;
            }
                
            Console.WriteLine($"Giving {soda.Name} out");
            money -= soda.Price;
            Console.WriteLine($"Giving {money} out in change");
            money = 0;
            soda.Nr--;
        }

        private void SmsOrderSoda(string sodaName)
        {
            if (!inventory.ContainsKey(sodaName) || inventory[sodaName].Nr == 0) return;
            var soda = inventory[sodaName];
            Console.WriteLine($"Giving {soda.Name} out");
            inventory[sodaName].Nr--;

        }
        
        private void RecallCash()
        {
            Console.WriteLine($"Returning {money} to customer");
            money = 0;
        }
    }
}