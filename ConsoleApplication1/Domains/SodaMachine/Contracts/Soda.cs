namespace ConsoleApplication1.Domains.SodaMachine.Contracts
{
    public class Soda
    {
        public string Name { get; set; }
        public int Nr { get; set; }
        public int Price { get; set; }

    }
}